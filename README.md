# airsbot_launch

* Package arrangement
<pre>
/home/airsbot
├── airsbot_outdoor_robot
│
├── airsbot2_nav
│
├── airsbot_launch
</pre>

* System requirment 
  * Ubuntu 20.04
  * ROS1 Noetic
  * ROS2 Galactic

## 1. Environmnet setup

```
$ sudo apt install git
$ sudo apt-get install openssh-server
$ sudo apt-get install net-tools
$ git clone https://gitlab.com/hcchou/airsbot_launch.git
$ bash /home/airsbot/airsbot_launch/script/installation/ros1_installation.sh
$ bash /home/airsbot/airsbot_launch/script/installation/ros2_galastic_installation.sh
$ bash /home/airsbot/airsbot_launch/script/installation/deploy_airsbot.sh
$ bash /home/airsbot/airsbot_launch/script/installation/deploy_airsbot2.sh
$ bash /home/airsbot/airsbot_launch/script/installation/system_installation.sh
```

## 2. Launch drivers, including IMU, lidar and base MCU

```
$ sudo chmod 777 /dev/USB_IMU
$ sudo chmod 777 /dev/USB_MCU
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch run_driver_mobile.launch
```

## 3. Launch mapping module

* Sensors should work at this point.
  * Check topics **/rslidar_points** and **/imu/data**

* Published topics
  * Pointcloud for 3D map **/lio_sam/mapping/map_global**
  * Occupency grid for 2D map **/projected_map**

* Run mapping module

```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch run_mapping_mobile.launch
```

* Save map service. The destination directory of map is **/home/airsbot/temp/** in this example.

```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ rosservice call /lio_sam/save_map 0.0 "/home/airsbot/temp/"
```

* Convert to occumpency map. The destination directory of map is **/home/airsbot/temp/map.pgm and map.yaml** in this example.
```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch octomap_mapping_nodelet_with_scan_pose.launch tum_poses_file:=/home/airsbot/temp/transformations.pcd lidar_scan_dir:=/home/airsbot/temp/keyframe
$ rosrun map_server map_saver -f /home/airsbot/temp/map map:=/projected_overall_map
```

## 4. Launch localization module

* Sensors should work at this point.
  * Check topics **/rslidar_points** and **/imu/data**

* Published topics
  * 3D Pose **/lio_sam/localization/odom**
  * 2D Pose **/lio_sam/localization/projected_odom**
  * Laser scan **/scan**

* Subscribed topics
  * 2D Pose **/initialpose**

* Prepare maps

```
$ bash /home/airsbot/airsbot_launch/script/installation/prepare_maps.sh
```

* Create **map.yaml** and **map.pcd** soft links. Take **airs_outdoor_map** as an example.

```
$ ln -s /home/airsbot/maps/airs_outdoor_map.yaml /home/airsbot/maps/map.yaml
$ ln -s /home/airsbot/maps/airs_outdoor_map.pcd /home/airsbot/maps/map.pcd
```

* Run localization module

```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch run_localization_mobile.launch
```

## 5. Launch localization and navigation modules

* Sensors should work at this point.
  * Check topics **/rslidar_points** and **/imu/data**

* Prepare maps

```
$ bash /home/airsbot/airsbot_launch/script/installation/prepare_maps.sh
```

* Create **map.yaml** and **map.pcd** soft links. Take **airs_outdoor_map** as an example.

```
$ ln -s /home/airsbot/maps/airs_outdoor_map.yaml /home/airsbot/maps/map.yaml
$ ln -s /home/airsbot/maps/airs_outdoor_map.pcd /home/airsbot/maps/map.pcd
$ ln -s /home/airsbot/maps/airs_outdoor_map.pgm /home/airsbot/maps/map.pgm
```

* 1st terminal for localization module

```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch run_localization_mobile.launch
```

* 2nd terminal for ROS1/2 brdige

```
$ source /opt/ros/noetic/setup.bash
$ source /home/airsbot/airsbot_outdoor_robot/devel/setup.bash
$ roslaunch airsbot_launch run_ros1_bridge.launch
```

* 3rd terminal for ROS2/1 brdige

```
$ source /opt/ros/noetic/setup.bash
$ source /opt/ros/galactic/setup.bash
$ ros2 run ros1_bridge parameter_bridge
```

* 4th terminal for navigation module

```
$ source /opt/ros/galactic/setup.bash
$ source /home/airsbot/airsbot2_nav/install/local_setup.bash 
$ ros2 launch airsbot2_launch airsbot2_navigation_launch.py
```

* Send navigation command through ROS2 action
```
$ source /opt/ros/galactic/setup.bash
$ source /home/airsbot/airsbot2_nav/install/local_setup.bash 
$ ros2 action send_goal /navigate_to_pose nav2_msgs/action/NavigateToPose "pose: {header: {frame_id: map}, pose: {position: {x: 4.6, y: -0.603, z: 0.0}, orientation:{x: 0.0, y: 0.0, z: 0.0, w: 1.0}}}"
```
