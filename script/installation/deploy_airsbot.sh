#!/bin/sh

source /opt/ros/noetic/setup.bash

mkdir -p /home/airsbot/airsbot_outdoor_robot/src
sudo rm -r /home/airsbot/airsbot_outdoor_robot/
mkdir -p /home/airsbot/airsbot_outdoor_robot/src
cd /home/airsbot/airsbot_outdoor_robot/src
git clone https://gitlab.com/hcchou/airsbot_outdoor_robot.git

cd /home/airsbot/airsbot_outdoor_robot
catkin_make
