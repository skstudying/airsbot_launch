#!/bin/sh

# ROS noetic.
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install -y curl # if you haven't already installed curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install -y ros-noetic-desktop-full
sudo apt install -y python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
sudo apt install -y python3-rosdep
sudo rosdep init
rosdep update

# GTSAM.
sudo add-apt-repository ppa:borglab/gtsam-release-4.0
sudo apt install -y libgtsam-dev libgtsam-unstable-dev

# Serial.
sudo apt-get install -y ros-noetic-serial

# RSLidar.
sudo apt-get install -y libpcap-dev

# glog and gflag.
sudo apt-get install -y libgoogle-glog-dev
sudo apt-get install -y libgflags-dev

# Pointcloud-to-laserscan.
sudo apt-get install -y ros-noetic-pointcloud-to-laserscan

# libdw
sudo apt-get install -y libdw-dev

# octomap-server.
sudo apt-get install -y ros-noetic-octomap-server

# map server.
sudo apt-get install -y ros-noetic-map-server

# Ceres.
cd ~/Downloads/
wget http://ceres-solver.org/ceres-solver-2.0.0.tar.gz
tar zxf ceres-solver-2.0.0.tar.gz
mkdir ceres-bin
cd ceres-bin
cmake ../ceres-solver-2.0.0
make
make test
sudo make install

# Livox.
cd ~/Downloads/
git clone https://github.com/Livox-SDK/Livox-SDK.git
cd Livox-SDK
cd build && cmake ..
make
sudo make install
