#!/bin/sh

source /opt/ros/galactic/setup.bash

mkdir -p /home/airsbot/airsbot2_nav/src
sudo rm -r /home/airsbot/airsbot2_nav/
mkdir -p /home/airsbot/airsbot2_nav/src
cd /home/airsbot/airsbot2_nav/src
git clone https://gitlab.com/hcchou/airsbot2_nav.git

cd /home/airsbot/airsbot2_nav
colcon build