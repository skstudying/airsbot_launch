#!/bin/sh

locale  # check for UTF-8
sudo apt update && sudo apt install locales
sudo locale-gen en_US en_US.UTF-8
sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
export LANG=en_US.UTF-8
locale  # verify settings

sudo apt install -y software-properties-common
sudo add-apt-repository universe

sudo apt update && sudo apt install -y curl
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

sudo apt update
sudo apt upgrade

sudo apt install -y ros-galactic-desktop
sudo apt install -y ros-dev-tools

# ros2 nivigation.
sudo apt install -y ros-galactic-navigation2
sudo apt install -y ros-galactic-nav2-bringup
sudo apt install -y ros-galactic-nav2-common

# ros1 bridge.
sudo apt-get install -y ros-galactic-ros1-bridge

# libg2o, for teb_local_planner.
sudo apt-get install -y ros-galactic-libg2o

# Set ROS_DOMAIN_ID
# echo "export ROS_DOMAIN_ID=5" >> ~/.bashrc

