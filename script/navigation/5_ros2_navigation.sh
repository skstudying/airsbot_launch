#!/bin/sh

export ROS_DOMAIN_ID=5
source /opt/ros/galactic/setup.bash
source /home/airsbot/airsbot2_nav/install/local_setup.bash 
ros2 launch airsbot2_launch airsbot2_navigation_launch.py
